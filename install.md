# Install

## parmetrage

- ouvrir le projet dans VS Code

- creer un ficher .env.local à la racine 
dans ce ficher copier :
APP_ENV=dev
APP_DEBUG=1
APP_SECRET=56561449e41dc318e32cc6bf305165f0
DATABASE_URL="mysql://root:@127.0.0.1:3306/airneis?serverVersion=8&charset=utf8mb4"

-créer un fichier .php-version à la racine du prjet
dans ce fichier : //ceci implique que tu ai PHP 8.1
8.1.*

## Lancer le projet
lancer wamp
Ouvrir 3 terminaux dans VSCode
- dans le premier lancer : symfony serve
ça va exposer le site sur le port 8000 : http://127.0.0.1:8000

- dans le deuxieme terminal lancer : yarn watch 
ceci va compiler les asset et permettre l'affichage avec javascript et le css

- dans le troisieme on va lancer les commmandes de lancement
symfony console doctrine:database:create        // pour creer la base de donnée airneis
symfony console doctrine:migration:migrate      // pour créer les tables

- dans le dossier utils il y a fixture.sql
copier le code et l'executer sur la base de données airneis dans wamp

## le site 
a partir de la le projet est fonctionnel
les trois routes importantes :
http://127.0.0.1:8000/airneis/                  //route pour le front de la boutique
http://127.0.0.1:8000/admin/airneis             //route pour le dashbord de l'admin
http://127.0.0.1:8000/api                       //c'est la route la plus importante pour toi :
meme si ce sont les accés génériques tu peux t'entrainer avec. je ferais des controllers plus precis plus tard

tu peux deja avancer sur l'app avec ça
j'ai terminé le panier et j'ajouterais des infos dans ce fichier regulierement, surtout pour les nouvelles fixtures que je mettrais en place