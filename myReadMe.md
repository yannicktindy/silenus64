composer require symfony/maker-bundle --dev

composer require symfony/webpack-encore-bundle
nvm install 18.12.0
nvm use 18.12.0
yarn add sass-loader@18
yarn remove sass-loader
yarn add sass-loader@^13.0.0
yarn add @symfony/webpack-encore --dev
yarn add file-loader

composer require twig
