<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240527133112 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE arns_adress (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, number VARCHAR(10) NOT NULL, name VARCHAR(255) NOT NULL, zip VARCHAR(10) NOT NULL, city VARCHAR(20) NOT NULL, country VARCHAR(20) NOT NULL, INDEX IDX_86AC9EB7A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE arns_basket (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, arns_adress_id INT DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', status VARCHAR(20) NOT NULL, INDEX IDX_F8060972A76ED395 (user_id), INDEX IDX_F806097296F26574 (arns_adress_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE arns_category (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(20) NOT NULL, img_name VARCHAR(20) DEFAULT NULL, file VARCHAR(255) DEFAULT NULL, slug VARCHAR(20) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE arns_line (id INT AUTO_INCREMENT NOT NULL, arns_basket_id INT DEFAULT NULL, product_id INT DEFAULT NULL, quantity INT NOT NULL, INDEX IDX_5F486C83995678A (arns_basket_id), INDEX IDX_5F486C834584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE arns_matter (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(20) NOT NULL, img_name VARCHAR(20) DEFAULT NULL, file VARCHAR(255) DEFAULT NULL, slug VARCHAR(20) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE arns_product (id INT AUTO_INCREMENT NOT NULL, category_id INT DEFAULT NULL, matter_id INT DEFAULT NULL, name VARCHAR(20) NOT NULL, description LONGTEXT NOT NULL, price DOUBLE PRECISION NOT NULL, quantity INT NOT NULL, slug VARCHAR(20) NOT NULL, created_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_AA4CFC5012469DE2 (category_id), INDEX IDX_AA4CFC50D614E59F (matter_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE arns_product_image (id INT AUTO_INCREMENT NOT NULL, product_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, file VARCHAR(255) NOT NULL, INDEX IDX_CB62625B4584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE arns_slide (id INT AUTO_INCREMENT NOT NULL, img_name VARCHAR(255) DEFAULT NULL, file VARCHAR(255) DEFAULT NULL, name VARCHAR(20) NOT NULL, description LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, firstname VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, mobile VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_IDENTIFIER_EMAIL (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE arns_adress ADD CONSTRAINT FK_86AC9EB7A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE arns_basket ADD CONSTRAINT FK_F8060972A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE arns_basket ADD CONSTRAINT FK_F806097296F26574 FOREIGN KEY (arns_adress_id) REFERENCES arns_adress (id)');
        $this->addSql('ALTER TABLE arns_line ADD CONSTRAINT FK_5F486C83995678A FOREIGN KEY (arns_basket_id) REFERENCES arns_basket (id)');
        $this->addSql('ALTER TABLE arns_line ADD CONSTRAINT FK_5F486C834584665A FOREIGN KEY (product_id) REFERENCES arns_product (id)');
        $this->addSql('ALTER TABLE arns_product ADD CONSTRAINT FK_AA4CFC5012469DE2 FOREIGN KEY (category_id) REFERENCES arns_category (id)');
        $this->addSql('ALTER TABLE arns_product ADD CONSTRAINT FK_AA4CFC50D614E59F FOREIGN KEY (matter_id) REFERENCES arns_matter (id)');
        $this->addSql('ALTER TABLE arns_product_image ADD CONSTRAINT FK_CB62625B4584665A FOREIGN KEY (product_id) REFERENCES arns_product (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE arns_adress DROP FOREIGN KEY FK_86AC9EB7A76ED395');
        $this->addSql('ALTER TABLE arns_basket DROP FOREIGN KEY FK_F8060972A76ED395');
        $this->addSql('ALTER TABLE arns_basket DROP FOREIGN KEY FK_F806097296F26574');
        $this->addSql('ALTER TABLE arns_line DROP FOREIGN KEY FK_5F486C83995678A');
        $this->addSql('ALTER TABLE arns_line DROP FOREIGN KEY FK_5F486C834584665A');
        $this->addSql('ALTER TABLE arns_product DROP FOREIGN KEY FK_AA4CFC5012469DE2');
        $this->addSql('ALTER TABLE arns_product DROP FOREIGN KEY FK_AA4CFC50D614E59F');
        $this->addSql('ALTER TABLE arns_product_image DROP FOREIGN KEY FK_CB62625B4584665A');
        $this->addSql('DROP TABLE arns_adress');
        $this->addSql('DROP TABLE arns_basket');
        $this->addSql('DROP TABLE arns_category');
        $this->addSql('DROP TABLE arns_line');
        $this->addSql('DROP TABLE arns_matter');
        $this->addSql('DROP TABLE arns_product');
        $this->addSql('DROP TABLE arns_product_image');
        $this->addSql('DROP TABLE arns_slide');
        $this->addSql('DROP TABLE user');
    }
}
