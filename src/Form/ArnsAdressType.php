<?php

namespace App\Form;

use App\Entity\ArnsAdress;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

class ArnsAdressType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('number', TextType::class, [
                'label' => 'Numéro',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Entrez le numéro de l\'adresse'
                ],
                'constraints' => [
                    new NotBlank(['message' => 'Le numéro de l\'adresse est obligatoire.']),
                    new Regex([
                        'pattern' => '/^\d+$/',
                        'message' => 'Le numéro de l\'adresse doit être composé uniquement de chiffres.'
                    ])
                ]
            ])
            ->add('name', TextType::class, [
                'label' => 'Nom',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Entrez le nom de l\'adresse'
                ],
                'constraints' => [
                    new NotBlank(['message' => 'Le nom de l\'adresse est obligatoire.'])
                ]
            ])
            ->add('zip', TextType::class, [
                'label' => 'Code postal',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Entrez le code postal'
                ],
                'constraints' => [
                    new NotBlank(['message' => 'Le code postal est obligatoire.']),
                    new Length(['min' => 5, 'max' => 10, 'minMessage' => 'Le code postal doit faire au moins 5 caractères.', 'maxMessage' => 'Le code postal ne peut pas faire plus de 10 caractères.'])
                ]
            ])
            ->add('city', TextType::class, [
                'label' => 'Ville',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Entrez la ville'
                ],
                'constraints' => [
                    new NotBlank(['message' => 'La ville est obligatoire.'])
                ]
            ])
            ->add('country', TextType::class, [
                'label' => 'Pays',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Entrez le pays'
                ],
                'constraints' => [
                    new NotBlank(['message' => 'Le pays est obligatoire.'])
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ArnsAdress::class,
        ]);
    }
}
