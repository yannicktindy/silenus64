<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\ArnsMatterRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ArnsMatterRepository::class)]
#[ApiResource]
class ArnsMatter
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 20)]
    private ?string $name = null;

    #[ORM\Column(length: 20, nullable: true)]
    private ?string $imgName = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $file = null;

    #[ORM\OneToMany(targetEntity: ArnsProduct::class, mappedBy: 'matter')]
    private Collection $arnsProducts;

    #[ORM\Column(length: 20)]
    private ?string $slug = null;

    public function __construct()
    {
        $this->arnsProducts = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getImgName(): ?string
    {
        return $this->imgName;
    }

    public function setImgName(string $imgName): static
    {
        $this->imgName = $imgName;

        return $this;
    }

    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(?string $file): static
    {
        $this->file = $file;

        return $this;
    }

    /**
     * @return Collection<int, ArnsProduct>
     */
    public function getArnsProducts(): Collection
    {
        return $this->arnsProducts;
    }

    public function addArnsProduct(ArnsProduct $arnsProduct): static
    {
        if (!$this->arnsProducts->contains($arnsProduct)) {
            $this->arnsProducts->add($arnsProduct);
            $arnsProduct->setMatter($this);
        }

        return $this;
    }

    public function removeArnsProduct(ArnsProduct $arnsProduct): static
    {
        if ($this->arnsProducts->removeElement($arnsProduct)) {
            // set the owning side to null (unless already changed)
            if ($arnsProduct->getMatter() === $this) {
                $arnsProduct->setMatter(null);
            }
        }

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): static
    {
        $this->slug = $slug;

        return $this;
    }
}
