<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\ArnsBasketRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ArnsBasketRepository::class)]
#[ApiResource]
class ArnsBasket
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $createdAt = null;

    #[ORM\ManyToOne(inversedBy: 'baskets')]
    private ?User $user = null;

    #[ORM\OneToMany(targetEntity: ArnsLine::class, mappedBy: 'arnsBasket', cascade: ['persist', 'remove'])]
    private Collection $basketLines;

    #[ORM\Column(length: 20)]
    private ?string $status = null;

    #[ORM\ManyToOne(inversedBy: 'basket')]
    private ?ArnsAdress $arnsAdress = null;

    #[ORM\Column(nullable: true)]
    private ?float $total = null;

    public function __construct()
    {
        $this->basketLines = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection<int, ArnsLine>
     */
    public function getBasketLines(): Collection
    {
        return $this->basketLines;
    }

    public function addBasketLine(ArnsLine $basketLine): static
    {
        if (!$this->basketLines->contains($basketLine)) {
            $this->basketLines->add($basketLine);
            $basketLine->setArnsBasket($this);
        }

        return $this;
    }

    public function removeBasketLine(ArnsLine $basketLine): static
    {
        if ($this->basketLines->removeElement($basketLine)) {
            // set the owning side to null (unless already changed)
            if ($basketLine->getArnsBasket() === $this) {
                $basketLine->setArnsBasket(null);
            }
        }

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): static
    {
        $this->status = $status;

        return $this;
    }

    public function getArnsAdress(): ?ArnsAdress
    {
        return $this->arnsAdress;
    }

    public function setArnsAdress(?ArnsAdress $arnsAdress): static
    {
        $this->arnsAdress = $arnsAdress;

        return $this;
    }

    public function getTotal(): ?float
    {
        return $this->total;
    }

    public function setTotal(?float $total): static
    {
        $this->total = $total;

        return $this;
    }
}
