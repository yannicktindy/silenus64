<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\ArnsProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ArnsProductRepository::class)]
#[ApiResource]
class ArnsProduct
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 20)]
    private ?string $name = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $description = null;

    #[ORM\Column]
    private ?float $price = null;

    #[ORM\Column]
    private ?int $quantity = null;

    #[ORM\OneToMany(targetEntity: ArnsLine::class, mappedBy: 'product')]
    private Collection $arnsLines;

    #[ORM\ManyToOne(inversedBy: 'arnsProducts')]
    private ?ArnsCategory $category = null;

    #[ORM\ManyToOne(inversedBy: 'arnsProducts')]
    private ?ArnsMatter $matter = null;

    #[ORM\OneToMany(targetEntity: ArnsProductImage::class, mappedBy: 'product')]
    private Collection $arnsProductImages;

    #[ORM\Column(length: 20)]
    private ?string $slug = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $createdAt = null;

    public function __construct()
    {
        $this->arnsLines = new ArrayCollection();
        $this->arnsProductImages = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): static
    {
        $this->price = $price;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): static
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * @return Collection<int, ArnsLine>
     */
    public function getArnsLines(): Collection
    {
        return $this->arnsLines;
    }

    public function addArnsLine(ArnsLine $arnsLine): static
    {
        if (!$this->arnsLines->contains($arnsLine)) {
            $this->arnsLines->add($arnsLine);
            $arnsLine->setProduct($this);
        }

        return $this;
    }

    public function removeArnsLine(ArnsLine $arnsLine): static
    {
        if ($this->arnsLines->removeElement($arnsLine)) {
            // set the owning side to null (unless already changed)
            if ($arnsLine->getProduct() === $this) {
                $arnsLine->setProduct(null);
            }
        }

        return $this;
    }

    public function getCategory(): ?ArnsCategory
    {
        return $this->category;
    }

    public function setCategory(?ArnsCategory $category): static
    {
        $this->category = $category;

        return $this;
    }

    public function getMatter(): ?ArnsMatter
    {
        return $this->matter;
    }

    public function setMatter(?ArnsMatter $matter): static
    {
        $this->matter = $matter;

        return $this;
    }

    /**
     * @return Collection<int, ArnsProductImage>
     */
    public function getArnsProductImages(): Collection
    {
        return $this->arnsProductImages;
    }

    public function addArnsProductImage(ArnsProductImage $arnsProductImage): static
    {
        if (!$this->arnsProductImages->contains($arnsProductImage)) {
            $this->arnsProductImages->add($arnsProductImage);
            $arnsProductImage->setProduct($this);
        }

        return $this;
    }

    public function removeArnsProductImage(ArnsProductImage $arnsProductImage): static
    {
        if ($this->arnsProductImages->removeElement($arnsProductImage)) {
            // set the owning side to null (unless already changed)
            if ($arnsProductImage->getProduct() === $this) {
                $arnsProductImage->setProduct(null);
            }
        }

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): static
    {
        $this->slug = $slug;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeImmutable $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
