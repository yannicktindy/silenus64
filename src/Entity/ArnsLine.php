<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\ArnsLineRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ArnsLineRepository::class)]
#[ApiResource]
class ArnsLine
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?int $quantity = null;

    #[ORM\ManyToOne(inversedBy: 'basketLines', cascade: ['persist', 'remove'])]
    private ?ArnsBasket $arnsBasket = null;

    #[ORM\ManyToOne(inversedBy: 'arnsLines', cascade: ['persist'])]
    private ?ArnsProduct $product = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): static
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getArnsBasket(): ?ArnsBasket
    {
        return $this->arnsBasket;
    }

    public function setArnsBasket(?ArnsBasket $arnsBasket): static
    {
        $this->arnsBasket = $arnsBasket;

        return $this;
    }

    public function getProduct(): ?ArnsProduct
    {
        return $this->product;
    }

    public function setProduct(?ArnsProduct $product): static
    {
        $this->product = $product;

        return $this;
    }
}
