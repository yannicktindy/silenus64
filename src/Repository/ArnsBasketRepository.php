<?php

namespace App\Repository;

use App\Entity\ArnsBasket;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ArnsBasket>
 *
 * @method ArnsBasket|null find($id, $lockMode = null, $lockVersion = null)
 * @method ArnsBasket|null findOneBy(array $criteria, array $orderBy = null)
 * @method ArnsBasket[]    findAll()
 * @method ArnsBasket[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArnsBasketRepository extends ServiceEntityRepository
{
    private EntityManagerInterface $entityManager;

    public function __construct(
        ManagerRegistry $registry,
        EntityManagerInterface $entityManager,
        )
    {
        parent::__construct($registry, ArnsBasket::class);
        $this->entityManager = $entityManager;
    }

    public function save(ArnsBasket $basket): void
    {
        $this->entityManager->persist($basket);
        $this->entityManager->flush();
    }
    

    //    /**
    //     * @return ArnsBasket[] Returns an array of ArnsBasket objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('a')
    //            ->andWhere('a.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('a.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?ArnsBasket
    //    {
    //        return $this->createQueryBuilder('a')
    //            ->andWhere('a.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
