<?php

namespace App\Repository;

use App\Entity\ArnsLine;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ArnsLine>
 *
 * @method ArnsLine|null find($id, $lockMode = null, $lockVersion = null)
 * @method ArnsLine|null findOneBy(array $criteria, array $orderBy = null)
 * @method ArnsLine[]    findAll()
 * @method ArnsLine[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArnsLineRepository extends ServiceEntityRepository
{
    private EntityManagerInterface $entityManager;
    
    public function __construct(
        ManagerRegistry $registry,
        EntityManagerInterface $entityManager,
        )
    {
        parent::__construct($registry, ArnsLine::class);
        $this->entityManager = $entityManager;
    }

    public function save(ArnsLine $line): void
    {
        $this->entityManager->persist($line);
        $this->entityManager->flush();
    }

//    /**
//     * @return ArnsLine[] Returns an array of ArnsLine objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('a')
//            ->andWhere('a.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('a.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?ArnsLine
//    {
//        return $this->createQueryBuilder('a')
//            ->andWhere('a.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
