<?php

namespace App\Repository;

use App\Entity\ArnsProduct;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ArnsProduct>
 *
 * @method ArnsProduct|null find($id, $lockMode = null, $lockVersion = null)
 * @method ArnsProduct|null findOneBy(array $criteria, array $orderBy = null)
 * @method ArnsProduct[]    findAll()
 * @method ArnsProduct[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArnsProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ArnsProduct::class);
    }

    public function findByCategory($category, array $orderBy = null, $offset = null, $limit = null)
    {
        $qb = $this->createQueryBuilder('p')
            ->andWhere('p.category = :category')
            ->setParameter('category', $category)
            ->setFirstResult($offset)
            ->setMaxResults($limit);
    
        if ($orderBy) {
            foreach ($orderBy as $field => $order) {
                $qb->addOrderBy('p.' . $field, $order);
            }
        }
    
        return $qb->getQuery()->getResult();
    }

    public function findByMatter($matter, array $orderBy = null, $offset = null, $limit = null)
    {
        $qb = $this->createQueryBuilder('p')
            ->andWhere('p.matter = :matter')
            ->setParameter('matter', $matter)
            ->setFirstResult($offset)
            ->setMaxResults($limit);
    
        if ($orderBy) {
            foreach ($orderBy as $field => $order) {
                $qb->addOrderBy('p.' . $field, $order);
            }
        }
    
        return $qb->getQuery()->getResult();
    }

    //    /**
    //     * @return ArnsProduct[] Returns an array of ArnsProduct objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('a')
    //            ->andWhere('a.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('a.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?ArnsProduct
    //    {
    //        return $this->createQueryBuilder('a')
    //            ->andWhere('a.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
