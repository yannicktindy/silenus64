<?php

namespace App\Repository;

use App\Entity\ArnsAdress;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ArnsAdress>
 *
 * @method ArnsAdress|null find($id, $lockMode = null, $lockVersion = null)
 * @method ArnsAdress|null findOneBy(array $criteria, array $orderBy = null)
 * @method ArnsAdress[]    findAll()
 * @method ArnsAdress[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArnsAdressRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ArnsAdress::class);
    }

    //    /**
    //     * @return ArnsAdress[] Returns an array of ArnsAdress objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('a')
    //            ->andWhere('a.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('a.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?ArnsAdress
    //    {
    //        return $this->createQueryBuilder('a')
    //            ->andWhere('a.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
