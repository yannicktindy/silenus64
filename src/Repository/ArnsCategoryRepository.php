<?php

namespace App\Repository;

use App\Entity\ArnsCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ArnsCategory>
 *
 * @method ArnsCategory|null find($id, $lockMode = null, $lockVersion = null)
 * @method ArnsCategory|null findOneBy(array $criteria, array $orderBy = null)
 * @method ArnsCategory[]    findAll()
 * @method ArnsCategory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArnsCategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ArnsCategory::class);
    }



    //    /**
    //     * @return ArnsCategory[] Returns an array of ArnsCategory objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('a')
    //            ->andWhere('a.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('a.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?ArnsCategory
    //    {
    //        return $this->createQueryBuilder('a')
    //            ->andWhere('a.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
