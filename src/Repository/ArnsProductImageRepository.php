<?php

namespace App\Repository;

use App\Entity\ArnsProductImage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ArnsProductImage>
 *
 * @method ArnsProductImage|null find($id, $lockMode = null, $lockVersion = null)
 * @method ArnsProductImage|null findOneBy(array $criteria, array $orderBy = null)
 * @method ArnsProductImage[]    findAll()
 * @method ArnsProductImage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArnsProductImageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ArnsProductImage::class);
    }

    //    /**
    //     * @return ArnsProductImage[] Returns an array of ArnsProductImage objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('a')
    //            ->andWhere('a.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('a.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?ArnsProductImage
    //    {
    //        return $this->createQueryBuilder('a')
    //            ->andWhere('a.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
