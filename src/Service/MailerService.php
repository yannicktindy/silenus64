<?php

namespace App\Service;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

class MailerService
{
    private $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public function sendRegistrationEmail(string $to, string $username): void
    {
        $email = (new TemplatedEmail())
            ->from(new Address('no-reply@example.com', 'Airnes Shop'))
            ->to(new Address($to, $username))
            ->subject('Welcome to Airnes Shop!')
            ->htmlTemplate('emails/registration.html.twig')
            ->context([
                'username' => $username,
            ]);
        $this->mailer->send($email);
    }

    public function sendRenewPswrdEmail(string $to, string $username, string $url): void
    {
        $email = (new TemplatedEmail())
        ->from(new Address('no-reply@example.com', 'Airnes Shop'))
        ->to(new Address($to, $username))
        ->subject('Welcome to Airnes Shop!')
        ->htmlTemplate('emails/renewPswrd.html.twig')
        ->context([
            'username' => $username,
            'url' => $url,
        ]);
        $this->mailer->send($email);
    } 
}

