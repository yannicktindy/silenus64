<?php

namespace App\Controller\Admin\Airneis;

use App\Entity\ArnsMatter;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ArnsMatterCrudController extends AbstractCrudController
{
    public const ACTION_DUPLICATE = 'duplicate';
    public const TEST_BASE_PATH = 'uploads/img/category';
    public const TEST_UPLOAD_PATH = 'public/' . self::TEST_BASE_PATH;

    public static function getEntityFqcn(): string
    {
        return ArnsMatter::class;
    }

    public function deleteEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        if ($entityInstance instanceof ArnsMatter) {
            // Récupérer le chemin de l'image à supprimer
            $imagePath = $this::TEST_UPLOAD_PATH . '/' . $entityInstance->getImgName();
    
            // Supprimer le fichier image
            if (file_exists($imagePath)) {
                unlink($imagePath);
            }
        }
    
        // Supprimer l'entité
        $entityManager->remove($entityInstance);
        $entityManager->flush();
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')
                ->onlyOnIndex(),
            TextField::new('name'),
            TextField::new('slug')
                ->setLabel('Slug')
                ->setRequired(false),
            TextField::new('imgName')
                ->setLabel('Image Name')
                ->setRequired(false),
            ImageField::new('file')
                ->setLabel('Sélectionner une image')
                ->setUploadDir($this::TEST_UPLOAD_PATH)
                ->setUploadedFileNamePattern($this::TEST_BASE_PATH.'/'.'[randomhash].[extension]')
                ->setRequired(false),
        ];
    }
}
