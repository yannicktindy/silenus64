<?php

namespace App\Controller\Admin\Airneis;

use App\Entity\ArnsProduct;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\SlugField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ArnsProductCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return ArnsProduct::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->setLabel('ID'),
            TextField::new('name')->setLabel('Name'),
            TextEditorField::new('description')->setLabel('Description'),
            NumberField::new('price')->setLabel('Price'),
            NumberField::new('quantity')->setLabel('Quantity'),
            AssociationField::new('category')->setLabel('Category'),
            AssociationField::new('matter')->setLabel('Matter'),
            TextField::new('slug')->setLabel('Slug'),
            DateTimeField::new('createdAt')->setLabel('Created At'),
            // Add more fields as needed
        ];
    }
}
