<?php

namespace App\Controller\Admin\Airneis;

use App\Controller\Admin\Airneis\ArnsCategoryCrudController as AirneisArnsCategoryCrudController;
use App\Controller\Admin\Airneis\ArnsMatterCrudController as AirneisArnsMatterCrudController;
use App\Controller\Admin\Airneis\ArnsProductCrudController as AirneisArnsProductCrudController;
use App\Controller\Admin\Airneis\ArnsSlideCrudController as AirneisArnsSlideCrudController;
use App\Controller\Admin\Airneis\UserCrudController as AirneisUserCrudController;
use App\Entity\ArnsBasket;
use App\Entity\ArnsCategory;
use App\Entity\ArnsMatter;
use App\Entity\ArnsProduct;
use App\Entity\ArnsProductImage;
use App\Entity\ArnsSlide;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AirneisDashboardController extends AbstractDashboardController
{
    #[Route('/admin/airneis', name: 'admin_airneis')]
    public function index(): Response
    {
        return $this->render('@EasyAdmin/page/content.html.twig', [
            // Passer les variables nécessaires à EasyAdmin
            'controller' => $this,
            'easyadmin' => [
                'dashboard' => true,
            ],
        ]);
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Administration Airneis');
    }

    public function configureActions(): Actions
    {
        return parent::configureActions()
            ->add(Crud::PAGE_INDEX, Action::DETAIL);
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToRoute('Retour à l\'accueil', 'fa fa-home', 'app_airneis');
        yield MenuItem::linkToDashboard('Panneau d\'administration', 'fa fa-dashboard');
        yield MenuItem::linkToRoute('Administration des panier', 'fa fa-dashboard', 'app_arneis_admin_basket');
        yield MenuItem::linkToRoute('Panneau des statistiques', 'fa fa-dashboard', 'app_arneis_admin_statistic');

        yield MenuItem::subMenu('Administration Utilisateurs', 'fa-solid fa-cubes')->setSubItems([
            MenuItem::linkToCrud('Utilisateurs', 'fas fa-eye', User::class)
                ->setController(UserCrudController::class),
            MenuItem::linkToCrud('Paniers', 'fas fa-eye', ArnsBasket::class)
                ->setController(ArnsBasketCrudController::class),
        ]);

        yield MenuItem::subMenu('Administration Produits', 'fa-solid fa-cubes')->setSubItems([
            MenuItem::linkToCrud('Produits', 'fas fa-eye', ArnsProduct::class)
                ->setController(AirneisArnsProductCrudController::class),
            MenuItem::linkToCrud('Image Produits', 'fas fa-eye', ArnsProductImage::class)
                ->setController(ArnsProductImageCrudController::class),    
            MenuItem::linkToCrud('Catégories', 'fas fa-eye', ArnsCategory::class)
                ->setController(AirneisArnsCategoryCrudController::class),
            MenuItem::linkToCrud('Matières', 'fas fa-eye', ArnsMatter::class)
                ->setController(AirneisArnsMatterCrudController::class),
        ]);

        yield MenuItem::subMenu('Administration Slides', 'fa-solid fa-cubes')->setSubItems([
            MenuItem::linkToCrud('Slidess', 'fas fa-eye', ArnsSlide::class)
                ->setController(AirneisArnsSlideCrudController::class),
        ]);
    }
}