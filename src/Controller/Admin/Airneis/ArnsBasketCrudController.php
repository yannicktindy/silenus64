<?php

namespace App\Controller\Admin\Airneis;

use App\Entity\ArnsBasket;
use App\Enum\ArnsBasketStatus;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ArnsBasketCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return ArnsBasket::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            DateTimeField::new('createdAt'),
            TextField::new('user.fullName', 'User'),
            ChoiceField::new('status')
                ->setChoices(array_combine(
                    array_map(fn(ArnsBasketStatus $status) => $status->value, ArnsBasketStatus::cases()),
                    array_map(fn(ArnsBasketStatus $status) => $status->name, ArnsBasketStatus::cases())
                ))
                ->renderAsBadges([
                    ArnsBasketStatus::Paid->value => 'success',
                    ArnsBasketStatus::Preparation->value => 'info',
                    ArnsBasketStatus::Abandoned->value => 'danger',
                    ArnsBasketStatus::Shipped->value => 'warning',
                ]),
        ];
    }
}
