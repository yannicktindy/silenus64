<?php

namespace App\Controller\Admin\Airneis;


use App\Entity\ArnsCategory;
use App\Entity\ArnsProduct;
use App\Enum\ArnsBasketStatus;
use App\Repository\ArnsBasketRepository;
use App\Repository\ArnsSlideRepository;
use App\Repository\ArnsCategoryRepository;
use App\Repository\ArnsMatterRepository;
use App\Repository\ArnsProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class BoardController extends AbstractController
{
    #[Route('/airneis/admin/basket', name: 'app_arneis_admin_basket')]
    public function adminBasket(
        ArnsBasketRepository $basketRepository
        
    ): Response
    {
        $basketPaid = $basketRepository->findBy(['status' => 'Payé']);
        $basketPreparation = $basketRepository->findBy(['status' => 'Préparation']);
        $allbaskets = $basketRepository->findAll();

        return $this->render('admin/airneis/basket.html.twig', [
            'controller_name' => 'AirnesController',
            'basketPaid' => $basketPaid,
            'basketPreparation' => $basketPreparation,
            'allBaskets' => $allbaskets
        ]);
    }

    #[Route('/airneis/admin/statistic', name: 'app_arneis_admin_statistic')]
    public function adminstatistic(
        ArnsSlideRepository $slideRepository,
        ArnsCategoryRepository $categoryRepository,
        ArnsMatterRepository $matterRepository
    ): Response
    {

        return $this->render('admin/airneis/statistic.html.twig', [
            'controller_name' => 'AirnesController',

        ]);
    }

    #[Route('/airneis/admin/to-preparation/{basketId}', name: 'app_arneis_admin_basket_preparation')]
    public function preparation(
        int $basketId,
        ArnsBasketRepository $basketRepository,
        EntityManagerInterface $entityManager
    ): Response
    {
        $basket = $basketRepository->find($basketId);
    
        if ($basket) {
            $basket->setStatus(ArnsBasketStatus::Preparation->value);
            $entityManager->flush();
        }
    
        return $this->redirectToRoute('app_arneis_admin_basket');
    }
    
    #[Route('/airneis/admin/to-shipped/{basketId}', name: 'app_arneis_admin_basket_shipped')]
    public function shipped(
        int $basketId,
        ArnsBasketRepository $basketRepository,
        EntityManagerInterface $entityManager
    ): Response
    {
        $basket = $basketRepository->find($basketId);
    
        if ($basket) {
            $basket->setStatus(ArnsBasketStatus::Shipped->value);
            $entityManager->flush();
        }
    
        return $this->redirectToRoute('app_arneis_admin_basket');
    }

    #[Route('/airneis/admin/to-payed/{basketId}', name: 'app_arneis_admin_basket_payed')]
    public function payed(
        int $basketId,
        ArnsBasketRepository $basketRepository,
        EntityManagerInterface $entityManager
    ): Response
    {
        $basket = $basketRepository->find($basketId);
    
        if ($basket) {
            $basket->setStatus(ArnsBasketStatus::Paid->value);
            $entityManager->flush();
        }
    
        return $this->redirectToRoute('app_arneis_admin_basket');
    }
}    