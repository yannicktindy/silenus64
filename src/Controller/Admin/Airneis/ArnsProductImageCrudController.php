<?php

namespace App\Controller\Admin\Airneis;

use App\Entity\ArnsProduct;
use App\Entity\ArnsProductImage;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ArnsProductImageCrudController extends AbstractCrudController
{
    public const ACTION_DUPLICATE = 'duplicate';
    public const TEST_BASE_PATH = 'uploads/img/product';
    public const TEST_UPLOAD_PATH = 'public/' . self::TEST_BASE_PATH;

    public static function getEntityFqcn(): string
    {
        return ArnsProductImage::class;
    }

    public function deleteEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        if ($entityInstance instanceof ArnsProductImage) {
            // Récupérer le chemin de l'image à supprimer
            $imagePath = $this::TEST_UPLOAD_PATH . '/' . $entityInstance->getName();
    
            // Supprimer le fichier image
            if (file_exists($imagePath)) {
                unlink($imagePath);
            }
        }
    
        // Supprimer l'entité
        $entityManager->remove($entityInstance);
        $entityManager->flush();
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')
                ->onlyOnIndex(),
            TextField::new('name'),    
            ImageField::new('file')
                ->setLabel('Sélectionner une image')
                ->setUploadDir($this::TEST_UPLOAD_PATH)
                ->setUploadedFileNamePattern($this::TEST_BASE_PATH.'/'.'[randomhash].[extension]')
                ->setRequired(true),
            AssociationField::new('product')
                ->setLabel('Product')
                ->setFormTypeOptions([
                    'class' => ArnsProduct::class,
                    'choice_label' => 'name',
                ]),
        ];
    }
}
