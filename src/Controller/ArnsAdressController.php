<?php

namespace App\Controller;

use App\Entity\ArnsAdress;
use App\Form\ArnsAdressType;
use App\Repository\ArnsAdressRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/arns/adress')]
class ArnsAdressController extends AbstractController
{
    #[Route('/', name: 'app_arns_adress_index', methods: ['GET'])]
    public function index(ArnsAdressRepository $arnsAdressRepository): Response
    {
        return $this->render('arns_adress/index.html.twig', [
            'arns_adresses' => $arnsAdressRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_arns_adress_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $arnsAdress = new ArnsAdress();
        $form = $this->createForm(ArnsAdressType::class, $arnsAdress);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($arnsAdress);
            $entityManager->flush();

            return $this->redirectToRoute('app_arns_adress_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('arns_adress/new.html.twig', [
            'arns_adress' => $arnsAdress,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_arns_adress_show', methods: ['GET'])]
    public function show(ArnsAdress $arnsAdress): Response
    {
        return $this->render('arns_adress/show.html.twig', [
            'arns_adress' => $arnsAdress,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_arns_adress_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, ArnsAdress $arnsAdress, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(ArnsAdressType::class, $arnsAdress);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_arns_adress_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('arns_adress/edit.html.twig', [
            'arns_adress' => $arnsAdress,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_arns_adress_delete', methods: ['POST'])]
    public function delete(Request $request, ArnsAdress $arnsAdress, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$arnsAdress->getId(), $request->request->get('_token'))) {
            $entityManager->remove($arnsAdress);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_arns_adress_index', [], Response::HTTP_SEE_OTHER);
    }
}
