<?php

namespace App\Controller\airnes;

use App\Entity\ArnsBasket;
use App\Entity\ArnsCategory;
use App\Entity\ArnsLine;
use App\Entity\ArnsProduct;
use App\Enum\ArnsBasketStatus;
use App\Repository\ArnsBasketRepository;
use App\Repository\ArnsSlideRepository;
use App\Repository\ArnsCategoryRepository;
use App\Repository\ArnsLineRepository;
use App\Repository\ArnsMatterRepository;
use App\Repository\ArnsProductRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class ArnsBasketController extends AbstractController
{

    private EntityManagerInterface $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager,
        )
    {
        $this->entityManager = $entityManager;
    }

    #[Route('/airneis/product/{productId}', name: 'app_arneis_product')]
    public function product(
        int $productId,
        ArnsProductRepository $productRepository,
        ArnsSlideRepository $slideRepository,
        SessionInterface $session,
        Request $request
    ): Response
    {
        $slides = $slideRepository->findAll();
        $product= $productRepository->findOneBy(['id' => $productId]);
        $message = 'C\'est le produit qu\'il vous faut !';

        $session = $request->getSession();

 

        if ($request->isMethod('POST')) {
            $requestedQuantity = $request->request->get('quantity');
    
            if ($session->has('panier')) {
                $panier = $session->get('panier');
                $lineFound = false;
                foreach ($panier as &$line) {
                    if ($line['product']->getId() == $product->getId()) {
                        $newQuantity = $line['quantity'] + $requestedQuantity;
                        if ($newQuantity > $product->getQuantity()) {
                            $line['quantity'] = $product->getQuantity();
                            $message = 'Seule la quantité en stock (' . $product->getQuantity() . ') a été ajoutée au panier.';
                        } else {
                            $line['quantity'] = $newQuantity;
                        }
                        $lineFound = true;
                        break;
                    }
                }
                if (!$lineFound) {
                    if ($requestedQuantity > $product->getQuantity()) {
                        $line = [
                            'product' => $product,
                            'quantity' => $product->getQuantity(),
                            'img' => $this->getOneImage($product->getCategory()->getSlug(), $product->getMatter()->getSlug())
                        ];
                        $message = 'Seule la quantité en stock (' . $product->getQuantity() . ') a été ajoutée au panier.';
                    } else {
                        $line = [
                            'product' => $product,
                            'quantity' => $requestedQuantity,
                            'img' => $this->getOneImage($product->getCategory()->getSlug(), $product->getMatter()->getSlug())
                        ];
                    }
                    $panier[] = $line;
                }
            } else {
                if ($requestedQuantity > $product->getQuantity()) {
                    $panier = [
                        [
                            'product' => $product,
                            'quantity' => $product->getQuantity(),
                            'img' => $this->getOneImage($product->getCategory()->getSlug(), $product->getMatter()->getSlug())
                        ]
                    ];
                    $message = 'Seule la quantité en stock (' . $product->getQuantity() . ') a été ajoutée au panier.';
                } else {
                    $panier = [
                        [
                            'product' => $product,
                            'quantity' => $requestedQuantity,
                            'img' => $this->getOneImage($product->getCategory()->getSlug(), $product->getMatter()->getSlug())
                        ]
                    ];
                }
            }
    
            $session->set('panier', $panier);
            if ($message === null) {
                $message = 'Le produit a été ajouté au panier';
            }
        }
        
        return $this->render('airneis/product.html.twig', [
            'controller_name' => 'AirnesController',
            'slides' => $slides,
            'product' => $product,
            'message' => $message
        ]);
    }
    
    #[Route('/airneis/basket', name: 'app_arns_basket')]
    public function index(
        // EntityManagerInterface $em,
        ArnsSlideRepository $slideRepository,
        SessionInterface $session,
        Request $request
    ): Response
    {
        $session = $request->getSession();
        $slides = $slideRepository->findAll();
        $panier = [];
        if ($session->has('panier')) {
            $panier = $session->get('panier');
    

        } 
        return $this->render('airneis/basket.html.twig', [
            'controller_name' => 'AirnesController',
            'panier' => $panier,
            'slides' => $slides,
        ]);
    }

    #[Route('/airneis/basket/order', name: 'app_arns_basket_order')]
    public function order(
        EntityManagerInterface $em,
        SessionInterface $session,
        ArnsLineRepository $lineRepository,
        ArnsBasketRepository $basketRepository,
        Request $request
    ): Response
    {
        $user = $this->getUser(); // Suppose que vous avez un utilisateur connecté
        // Récupérer le panier de la session
        $panier = $session->get('panier', []);
        // dump($panier);
    
        // Créer un nouveau panier en base de données
        $basket = new ArnsBasket();
        $basket->setCreatedAt(new \DateTimeImmutable());
        $basket->setUser($user);
        $basket->setStatus(ArnsBasketStatus::Paid->value);
    
        $total = 0;
        // Créer les lignes de panier en base de données
        foreach ($panier as $line) {
            $product = $this->entityManager->getRepository(ArnsProduct::class)
                 ->findOneBy(['id' => $line['product']->getId()]);
            $basketLine = new ArnsLine();
            $basketLine->setQuantity($line['quantity']);
            $basketLine->setProduct($product);
            $basketLine->setArnsBasket($basket);
            $lineRepository->save($basketLine);
            
            // Calculer le total
            $total += $line['quantity'] * $product->getPrice();

        }
        
        $basket->setTotal($total);
        $basketRepository->save($basket);
        
        // Supprimer le panier de la session
        $session->remove('panier');
        // $this->addFlash('success', 'Votre commande a été passée avec succès.');
        return $this->redirectToRoute('app_arneis_home');
    }

    #[Route('/airneis/remove-product/{productId}', name: 'app_arns_basket_remove_product')]
    public function removeProduct(
        int $productId,
        SessionInterface $session)
    {
        $panier = $session->get('panier', []);
        foreach ($panier as $index => $item) {
            if ($item['product']->getId() === $productId) {
                unset($panier[$index]);
                break;
            }
        }
        $session->set('panier', $panier);

        return $this->redirectToRoute("app_arns_basket");
    }

    #[Route('/airneis/remove-basket', name: 'app_arns_remove_basket')]
    public function removeBasket(SessionInterface $session)
    {
        $session->remove('panier');

        return $this->redirectToRoute("app_arns_basket");
    }

    public function getOneImage($categorySlug, $matterSlug) {
        $image = mt_rand(1, 4); // Génère un nombre entier aléatoire entre 1 et 4
        $imagePath = sprintf('/build/img/mapped/%s/%s/%d.jpg', $categorySlug, $matterSlug, $image);
        return $imagePath;
    }



}
