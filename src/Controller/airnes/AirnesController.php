<?php

namespace App\Controller\airnes;

use App\Entity\ArnsAdress;
use App\Entity\ArnsCategory;
use App\Entity\ArnsProduct;
use App\Entity\User;
use App\Form\ArnsAdressType;
use App\Form\RenewPswrdType;
use App\Repository\ArnsAdressRepository;
use App\Repository\ArnsSlideRepository;
use App\Repository\ArnsCategoryRepository;
use App\Repository\ArnsMatterRepository;
use App\Repository\ArnsProductRepository;
use App\Repository\UserRepository;
use App\Service\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class AirnesController extends AbstractController
{
    #[Route('/', name: 'app_arneis_ghost')]
    
    #[Route('/airneis/user/send-renew-pswrd/', name: 'app_arneis_user_send_renew_pswrd')]
    public function sendRenewPswrd(
        Request $request,
        UserRepository $userRepository,
        EntityManagerInterface $entityManager,
        MailerService $mailerService
    ): Response
    {
        $currentUserCredentials = $this->getUser()->getUserIdentifier();
        $user = $userRepository->findOneBy(['email' => $currentUserCredentials]);
        if ($user) {
            $randomToken = bin2hex(random_bytes(32));
            $currentDate = new \DateTime();
            // $url = $this->generateUrl('app_arneis_user_renew_pswrd', ['userToken' => $randomToken]);
            $url = "http://127.0.0.1:8000/airneis/user/renew-pswrd/" . $randomToken;
            $user->setToken($randomToken);
            $user->setTokentCreatedAt($currentDate);
            $entityManager->persist($user);
            $entityManager->flush();

            $mailerService->sendRenewPswrdEmail($currentUserCredentials, $user->getFullName(), $url);

        }
        return $this->redirectToRoute('app_arneis_home');
    }   

    #[Route('/airneis/user/renew-pswrd/{token}', name: 'app_arneis_user_renew_pswrd')]
    public function RenewPswrd(
        string $token,
        Request $request,
        UserRepository $userRepository,
        EntityManagerInterface $entityManager,
        UserPasswordHasherInterface $userPasswordHasher,
    ): Response
    {
        $currentUserCredentials = $this->getUser()->getUserIdentifier();
        $currentUser = $userRepository->findOneBy(['email' => $currentUserCredentials]);
        $currentUserToken = $currentUser->getToken();
        if (!$currentUser) {
            return $this->redirectToRoute('app_login');
        }

        if ($currentUserToken !== $token) {
            return $this->redirectToRoute('app_login');
        }

        $form = $this->createForm(RenewPswrdType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $currentUser->setPassword(
                $userPasswordHasher->hashPassword(
                $currentUser,
                $data['plainPassword']
            )
            );
            $currentUser->setToken(null);
            $entityManager->flush();
    

            return $this->redirectToRoute('app_logout');
        }

        return $this->render('airneis/renewPswrd.html.twig', [
            'renewPasswordForm' => $form,
        ]);
    }  

    #[Route('/airneis', name: 'app_arneis_home')]
    public function index(
        ArnsSlideRepository $slideRepository,
        ArnsCategoryRepository $categoryRepository,
        ArnsMatterRepository $matterRepository
    ): Response
    {
        $slides = $slideRepository->findAll();
        $categories = $categoryRepository->findAll();
        $matters = $matterRepository->findAll();
        return $this->render('airneis/index.html.twig', [
            'controller_name' => 'AirnesController',
            'slides' => $slides,
            'categories' => $categories,
            'matters' => $matters,
        ]);
    }

    #[Route('/airneis/cop/{code}', name: 'app_arneis_cop')]
    public function cop(
        string $code,
    ): Response
    {
        if ($code == '403') {
            $message = "vous n'avez rien à foutre ici";
        }    

        return $this->render('airneis/cop.html.twig', [
            'controller_name' => 'AirnesController',
            'message' => $message,
        ]);
    }

    #[Route('/airneis/user/delete-adress/{adressId}', name: 'app_arneis_user_delete_adress', methods: ['POST'])]
    public function delete(
        int $adressId,
        Request $request, 
        EntityManagerInterface $entityManager
        ): Response
    {
        $arnsAdress = $entityManager->getRepository(ArnsAdress::class)->find($adressId);
        if ($this->isCsrfTokenValid('delete'.$arnsAdress->getId(), $request->request->get('_token'))) {
            $entityManager->remove($arnsAdress);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_arneis_user_profil', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/airneis/user/edit-adress/{adressId}', name: 'app_arneis_user_edit_adress', methods: ['GET', 'POST'])]
    public function editAdress(
        int $adressId,
        Request $request, 
        EntityManagerInterface $entityManager,
        ArnsAdressRepository $arnsAdressRepository
        ): Response
    {

        $adress = $arnsAdressRepository->findOneBy(['id' => $adressId]);
        $form = $this->createForm(ArnsAdressType::class, $adress);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($adress);
            $entityManager->flush();

            return $this->redirectToRoute('app_arneis_user_profil', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('airneis/editAdress.html.twig', [
            'arns_adress' => $adress,
            'form' => $form,
        ]);
    }

    #[Route('/airneis/user/new-adress/{userId}', name: 'app_arneis_user_new_adress', methods: ['GET', 'POST'])]
    public function newAdress(
        int $userId,
        Request $request, 
        EntityManagerInterface $entityManager
        ): Response
    {
        $user = $this->getUser();
        $arnsAdress = new ArnsAdress();
        $form = $this->createForm(ArnsAdressType::class, $arnsAdress);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $arnsAdress->setUser($user);
            $entityManager->persist($arnsAdress);
            $entityManager->flush();

            return $this->redirectToRoute('app_arneis_user_profil', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('airneis/newAdress.html.twig', [
            'arns_adress' => $arnsAdress,
            'form' => $form,
        ]);
    }

    #[Route('/airneis/user/profil', name: 'app_arneis_user_profil')]
    public function profil(
        ArnsSlideRepository $slideRepository,
        ArnsCategoryRepository $categoryRepository,
        ArnsMatterRepository $matterRepository,
        UserRepository $userRepository,
        SessionInterface $session,
        UserInterface $user
    ): Response
    {
        if (!$this->getUser()) {
            return $this->redirectToRoute('app_arneis_cop', ['code' => '403']);
        }
        $panier = $session->get('panier');
        $user = $this->getUser();
        $credentials = $user->getUserIdentifier();
        $profil = $userRepository->findOneBy(['email' => $credentials]);
        $baskets = $profil->getBaskets();
        $adresses = $profil->getAdresses();
        // $oldBasket = $user->getBaskets();

        return $this->render('airneis/profil.html.twig', [
            'controller_name' => 'AirnesController',
            'profil' => $profil,
            'baskets' => $baskets,
            'panier' => $panier,
            'adresses' => $adresses,
        ]);
    }

    #[Route('/airneis/search', name: 'app_arneis_search')]
    public function serach(
        ArnsSlideRepository $slideRepository,
        ArnsCategoryRepository $categoryRepository,
        ArnsMatterRepository $matterRepository,
        ArnsProductRepository $productRepository,
        Request $request
    ): Response
    {
        $slides = $slideRepository->findAll();
        $categories = $categoryRepository->findAll();
        $matters = $matterRepository->findAll();

        $queryBuilder = $productRepository->createQueryBuilder('p');

        // Filtre par nom de produit
        $productName = $request->query->get('product_name');
        if ($productName) {
            $queryBuilder->andWhere('p.name LIKE :productName')
                ->setParameter('productName', '%'.$productName.'%');
        }
    
        // Filtre par catégorie
        $categoryId = $request->query->get('category_id');
        if ($categoryId) {
            $queryBuilder->andWhere('p.category = :categoryId')
                ->setParameter('categoryId', $categoryId);
        }
    
        // Filtre par matière
        $matterId = $request->query->get('matter_id');
        if ($matterId) {
            $queryBuilder->andWhere('p.matter = :matterId')
                ->setParameter('matterId', $matterId);
        }
    
        // Filtre par prix minimum
        $priceMin = $request->query->get('price_min');
        if ($priceMin) {
            $queryBuilder->andWhere('p.price >= :priceMin')
                ->setParameter('priceMin', $priceMin);
        }
    
        // Filtre par prix maximum
        $priceMax = $request->query->get('price_max');
        if ($priceMax) {
            $queryBuilder->andWhere('p.price <= :priceMax')
                ->setParameter('priceMax', $priceMax);
        }
    
        // Filtre par stock
        $inStock = $request->query->get('in_stock');
        if ($inStock) {
            $queryBuilder->andWhere('p.stockQuantity > 0');
        }

        // Limite la recherche à 10 produits
        $queryBuilder->setMaxResults(10);
    
        $productsFound = $queryBuilder->getQuery()->getResult();
        $products = [];
        foreach ($productsFound as $product) {
            $categorySlug = $product->getCategory()->getSlug();
            $matterSlug = $product->getMatter()->getSlug();
            $imagePath = $this->getOneIamge($categorySlug, $matterSlug);
            $products[] = [
                'product' => $product,
                'imagePath' => $imagePath
            ];
        }
        return $this->render('airneis/search.html.twig', [
            'controller_name' => 'AirnesController',
            'slides' => $slides,
            'categories' => $categories,
            'matters' => $matters,
            'products' => $products,
        ]);
    }

    #[Route('/airneis/product-category/{categorySlug}', name: 'app_arneis_category')]
    public function category(
        string $categorySlug,
        ArnsSlideRepository $slideRepository,
        ArnsCategoryRepository $categoryRepository,
        ArnsProductRepository $productRepository,
        Request $request,
        int $page = 1
    ): Response
    {
    $slides = $slideRepository->findAll();
    $category = $categoryRepository->findOneBy(['slug' => $categorySlug]);
    $productsByCateg = $productRepository->findByCategory($category);

    $products = [];
    foreach ($productsByCateg as $product) {
        $categorySlug = $product->getCategory()->getSlug();
        $matterSlug = $product->getMatter()->getSlug();
        $imagePath = $this->getOneIamge($categorySlug, $matterSlug);
        $products[] = [
            'product' => $product,
            'imagePath' => $imagePath
        ];
    }

    $productsPerPage = 3; // Nombre de produits par page
    $totalProducts = count($products);
    $totalPages = ceil($totalProducts / $productsPerPage);

    $offset = ($request->query->getInt('page', 1) - 1) * $productsPerPage;
    $paginatedProducts = array_slice($products, $offset, $productsPerPage);

    return $this->render('airneis/products.html.twig', [
        'controller_name' => 'AirnesController',
        'slides' => $slides,
        'products' => $paginatedProducts,
        'category' => $category,
        'currentPage' => $request->query->getInt('page', 1),
        'totalPages' => $totalPages,
    ]);
}

    #[Route('/airneis/product-matter/{matterSlug}', name: 'app_arneis_matter')]
    public function matter(
        ArnsSlideRepository $slideRepository,
        ArnsCategoryRepository $categoryRepository,
        ArnsMatterRepository $matterRepository
    ): Response
    {
        $slides = $slideRepository->findAll();
        $categories = $categoryRepository->findAll();
        $matters = $matterRepository->findAll();
        return $this->render('airneis/products.html.twig', [
            'controller_name' => 'AirnesController',
            'slides' => $slides,
            'categories' => $categories,
            'matters' => $matters
        ]);
    }

    // #[Route('/airneis/{productSlug}', name: 'app_arneis_product')]
    // public function product(
    //     string $productSlug,
    //     ArnsProductRepository $productRepository,
    //     SessionInterface $session
    // ): Response
    // {
    //     $product= $productRepository->findOneBy(['slug' => $productSlug]);
    //     return $this->render('airneis/products.html.twig', [
    //         'controller_name' => 'AirnesController',
    //         'product' => $product,
    //     ]);
    // }





    public function getOneIamge($categorySlug, $matterSlug) {
        $image = mt_rand(1, 4); // Génère un nombre entier aléatoire entre 1 et 4
        $imagePath = sprintf('/build/img/mapped/%s/%s/%d.jpg', $categorySlug, $matterSlug, $image);
        return $imagePath;
        // $imagePath contiendra quelque chose comme '/build/img/mapped/table/bois-clair/3.jpg'
        // $categorySlug = 'table';
        // $matterSlug = 'bois-clair';
        // $imagePath = $this->getOneImage($categorySlug, $matterSlug);
        // $imagePath contiendra quelque chose comme '/build/img/mapped/table/bois-clair/3.jpg'
    }

    public function getThreeImages($categorySlug, $matterSlug) {
        $images = [];
        for ($i = 1; $i <= 3; $i++) {
            $image = mt_rand(1, 4);
            $images[] = sprintf('/build/img/mapped/%s/%s/%d.jpg', $categorySlug, $matterSlug, $image);
        }
        return $images;
        // $images contiendra un tableau de 3 éléments, chacun contenant un chemin d'image
        // $categorySlug = 'table';
        // $matterSlug = 'bois-clair';
        // $images = $this->getThreeImages($categorySlug, $matterSlug);
        // $images contiendra un tableau de 3 éléments, chacun contenant un chemin d'image
    }
}
