<?php
// src/Controller/Api/ArnsController.php
namespace App\Controller\Api;

use App\Entity\ArnsCategory;
use App\Entity\ArnsProduct;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GetCollection;
use App\Repository\ArnsCategoryRepository;
use App\Repository\ArnsMatterRepository;
use App\Repository\ArnsProductRepository;
use App\Repository\ArnsSlideRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

#[ApiResource]
class ArnsController extends AbstractController
{
    #[Route('/api/get/hello', name: 'api_get_hello')]
    public function hello(): JsonResponse {
        return $this->json(['hello' => 'Hello flutter !']);  
    }

    #[Route('/api/get/get-slides', name: 'api_get_slides')]
    public function slides(
        ArnsSlideRepository $slideRepository
    ): JsonResponse {

        $sildes = $slideRepository->findAll();

        return $this->json($sildes);  
    }

    #[Route('/api/get/get-category-products/{categoryId}', name: 'api_get_category_product')]
    public function getCategoryProducts(
        int $categoryId,
        ArnsCategoryRepository $categoryRepository,
        ArnsProductRepository $productRepository
    ): JsonResponse {
        $category = $categoryRepository->findOneBy(['id' => $categoryId]);

        if (!$category) {
            return $this->json(['error' => 'Category not found'], 404);
        }

        $productsByCateg = $productRepository->findByCategory($category);

        $products = [];
        foreach ($productsByCateg as $product) {
            $categorySlug = $product->getCategory()->getSlug();
            $matterSlug = $product->getMatter()->getSlug();
            $imagePath = $this->getOneImage($categorySlug, $matterSlug);
            $products[] = [
                'product' => $product,
                'imagePath' => $imagePath
            ];
        }

        return $this->json($products);
    }

    #[Route('/api/get/get-category-products-paginate/{categoryId}/{page}', name: 'api_get_category_product_paginate')]
    public function getCategoryProductsPaginate(
        int $categoryId,
        int $page,
        ArnsCategoryRepository $categoryRepository,
        ArnsProductRepository $productRepository
    ): JsonResponse {
        $category = $categoryRepository->findOneBy(['id' => $categoryId]);
    
        if (!$category) {
            return $this->json(['error' => 'Category not found'], 404);
        }
    
        $productsPerPage = 3;
        $totalProducts = $productRepository->countByCategory($category);
        $totalPages = max(1, ceil($totalProducts / $productsPerPage));
    
        // Ensure the page is within the valid range
        $page = max(1, min($page, $totalPages));
    
        $offset = ($page - 1) * $productsPerPage;
        $productsByCateg = $productRepository->findByCategory($category, ['id' => 'ASC'], $offset, $productsPerPage);
    
        $products = [];
        foreach ($productsByCateg as $product) {
            $categorySlug = $product->getCategory()->getSlug();
            $matterSlug = $product->getMatter()->getSlug();
            $imagePath = $this->getOneImage($categorySlug, $matterSlug);
            $products[] = [
                'product' => $product,
                'imagePath' => $imagePath
            ];
        }
    
        $data = [
            'currentPage' => $page,
            'totalPages' => $totalPages,
            'products' => $products
        ];
    
        return $this->json($data);
    }

    #[Route('/api/get/get-matter-products-paginate/{matterId}/{page}', name: 'api_get_matter_product_paginate')]
    public function getMatterProductsPaginate(
        int $matterId,
        int $page,
        ArnsMatterRepository $matterRepository,
        ArnsProductRepository $productRepository
    ): JsonResponse {
        $category = $matterRepository->findOneBy(['id' => $matterId]);
    
        if (!$category) {
            return $this->json(['error' => 'Category not found'], 404);
        }
    
        $productsPerPage = 3;
        $totalProducts = $productRepository->countByMatter($category);
        $totalPages = max(1, ceil($totalProducts / $productsPerPage));

        // Ensure the page is within the valid range
        $page = max(1, min($page, $totalPages));

        $offset = ($page - 1) * $productsPerPage;
        $productsByMatter = $productRepository->findByMatter($category, ['id' => 'ASC'], $offset, $productsPerPage);

        $products = [];
        foreach ($productsByMatter as $product) {
            $categorySlug = $product->getCategory()->getSlug();
            $matterSlug = $product->getMatter()->getSlug();
            $imagePath = $this->getOneImage($categorySlug, $matterSlug);
            $products[] = [
                'product' => $product,
                'imagePath' => $imagePath
            ];
        }

        $data = [
            'currentPage' => $page,
            'totalPages' => $totalPages,
            'products' => $products
        ];

        return $this->json($data);

    }

    private function getOneImage(string $categorySlug, string $matterSlug): string
    {
        $image = mt_rand(1, 4); // Génère un nombre entier aléatoire entre 1 et 4
        return sprintf('/build/img/mapped/%s/%s/%d.jpg', $categorySlug, $matterSlug, $image);
    }

    private function getThreeImages(string $categorySlug, string $matterSlug): array
    {
        $images = [];
        for ($i = 1; $i <= 3; $i++) {
            $image = mt_rand(1, 4);
            $images[] = sprintf('/build/img/mapped/%s/%s/%d.jpg', $categorySlug, $matterSlug, $image);
        }
        return $images;
    }
}