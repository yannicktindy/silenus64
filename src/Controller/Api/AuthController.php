<?php
// src/Controller/AuthController.php
namespace App\Controller\Api;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AuthController extends AbstractController
{
    private $passwordHasher;
    private $entityManager;

    public function __construct(UserPasswordHasherInterface $passwordHasher, EntityManagerInterface $entityManager)
    {
        $this->passwordHasher = $passwordHasher;
        $this->entityManager = $entityManager;
    }

    #[Route('/api/get-token', name: 'get_token', methods: ['POST'])]
    public function getToken(Request $request, JWTTokenManagerInterface $JWTManager)
    {
    // Décoder le contenu JSON de la requête
    $data = json_decode($request->getContent(), true);

    // Vérifier si le décodage a réussi et si les champs nécessaires sont présents
    if (json_last_error() !== JSON_ERROR_NONE || !$data) {
        return new JsonResponse(['error' => 'Invalid JSON'], 400);
    }

    $email = $data['email'] ?? null;
    $password = $data['password'] ?? null;

    // Vérifier si l'email et le mot de passe sont fournis
    if (!$email || !$password) {
        return new JsonResponse(['error' => 'Email and password are required'], 400);
    }

    // Récupérer l'utilisateur par email
    $user = $this->getUserByEmail($email);

    // Vérifier si l'utilisateur existe
    if (!$user) {
        return new JsonResponse(['error' => 'User not found'], 404);
    }

    // Vérifier si le mot de passe est valide
    if (!$this->passwordHasher->isPasswordValid($user, $password)) {
        return new JsonResponse(['error' => 'Invalid credentials'], 401);
    }

    // Créer un token JWT pour l'utilisateur
    $token = $JWTManager->create($user);
    return new JsonResponse(['token' => $token]);
    }

    private function getUserByEmail($email)
    {
        return $this->entityManager->getRepository(User::class)->findOneByEmail($email);
    }
}