<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class SkynnController extends AbstractController
{
    #[Route('/skynn', name: 'app_skynn')]
    public function index(): Response
    {
        return $this->render('front/skynn/skynnBase.html.twig', [
            
        ]);
    }
}


