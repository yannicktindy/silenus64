<?php
// src/Enum/ArnsBasketStatus.php
namespace App\Enum;

enum ArnsBasketStatus: string
{
    case Paid = 'Payé';
    case Preparation = 'Préparation';
    case Abandoned = 'Abandonné';
    case Shipped = 'Expedié';
}