INSERT INTO `user` (`id`, `email`, `roles`, `password`, `firstname`, `lastname`, `mobile`) VALUES
(1, 'admin@airneis.com', '[\"ROLE_USER\", \"ROLE_ADMIN\", \"ROLE_SUPER_ADMIN\"]', '$2y$13$Ln8vkxXIUYj2RCfbuXGnDuGyNXzv29M/1d7syqwFcuG3H6QarwCxC', 'admin', 'admin', NULL),
(2, 'user@airneis.com', '[\"ROLE_USER\"]', '$2y$13$Ln8vkxXIUYj2RCfbuXGnDuGyNXzv29M/1d7syqwFcuG3H6QarwCxC', 'user', 'user', NULL);



INSERT INTO `arns_adress` (`id`, `user_id`, `number`, `name`, `zip`, `city`, `country`) VALUES
(1, 1, '117', 'rue annatole france', '69100', 'Villeurbanne', 'France');

INSERT INTO `arns_basket` (`id`, `user_id`, `created_at`, `status`, `arns_adress_id`) VALUES
(9, 1, '2024-05-23 19:11:21', 'Payé', NULL),
(10, 1, '2024-05-27 12:08:53', 'Payé', NULL);




INSERT INTO `arns_category` (`id`, `name`, `img_name`, `file`, `slug`) VALUES
(3, 'Table', 'imagename', 'uploads/img/category/16f3deb6c9f7649fa4b443f637a297348ce0311c.jpg', 'table'),
(4, 'Chaise', 'chaise', 'uploads/img/category/14cebddcb1b7c24d6c43137ed2ce345584bb4394.jpg', 'chaise'),
(5, 'Tabouret', 'tabouret', 'uploads/img/category/c910ecc9836c2ce79fd44bcdf1afc90d22b91452.jpg', 'tabouret');


INSERT INTO `arns_matter` (`id`, `name`, `img_name`, `file`, `slug`) VALUES
(1, 'Metal', 'metal', 'uploads/img/category/2fd2a5ee26e3dc3ce84be33bad30d0c1d63930cc.jpg', 'metal'),
(2, 'Bois Clair', 'bois-clair', 'uploads/img/category/77c37df64481d7714abd90ffcb671958dc802f1a.jpg', 'boisclair'),
(3, 'Bois Sombre', 'bois-sombre', 'uploads/img/category/1160f15c008f64de386fd27f5293f8cd7d225edc.jpg', 'boissombre');


INSERT INTO `arns_product` (`id`, `category_id`, `matter_id`, `name`, `description`, `price`, `quantity`, `slug`, `created_at`) VALUES
(2, 3, 2, 'Table MacGregor', 'Table MacGregor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin euismod eros justo, vel varius eros efficitur vel.', 588.91, 3, 'table-macgregor', '2024-03-15 17:42:09'),
(3, 3, 3, 'Table Eilidh', 'Table Eilidh. Nullam efficitur metus ut augue bibendum, vel mattis mi interdum. Donec eget tellus nec magna efficitur.', 256.87, 10, 'table-eilidh', '2024-01-22 08:24:36'),
(4, 3, 1, 'Table MacLeod', 'Table MacLeod. Praesent eu urna nec magna tincidunt condimentum. Etiam ac augue vel magna finibus tempus. Suspendisse.', 180.45, 4, 'table-macleod', '2024-04-30 14:18:52'),
(5, 3, 2, 'Table MacKenzie', 'Table MacKenzie. Cras id ipsum eget libero commodo lobortis. Donec mattis libero nec velit sagittis, vel vehicula.', 216.68, 1, 'table-mackenzie', '2024-02-08 11:05:23'),
(6, 3, 3, 'Table Finlay', 'Table Finlay. Morbi eget magna vel risus ultrices laoreet. Aliquam erat volutpat. Nulla facilisi. Phasellus feugiat.', 592.79, 6, 'table-finlay', '2024-05-01 09:37:41'),
(7, 3, 1, 'Table Morag', 'Table Morag. Sed ut risus nec neque efficitur molestie. Nulla facilisi. Vivamus euismod magna vel magna.', 412.89, 7, 'table-morag', '2024-04-18 21:53:14'),
(8, 3, 2, 'Table Cameron', 'Table Cameron. Aenean vel magna vel magna finibus condimentum. Pellentesque habitant morbi tristique senectus et netus.', 109.88, 4, 'table-cameron', '2024-03-27 16:29:06'),
(9, 4, 1, 'Chaise Lachlan', 'Chaise Lachlan. Fusce vel magna vel magna finibus condimentum. Proin id magna vel magna finibus.', 272.97, 8, 'chaise-lachlan', '2024-02-13 12:11:47'),
(10, 4, 2, 'Chaise MacGregor', 'Chaise MacGregor. Nulla facilisi. Vivamus euismod magna vel magna finibus condimentum. Proin id magna vel.', 333.03, 10, 'chaise-macgregor', '2024-05-05 19:04:28'),
(11, 4, 3, 'Chaise Eilidh', 'Chaise Eilidh. Sed ut risus nec neque efficitur molestie. Nulla facilisi. Vivamus euismod magna vel.', 120.8, 9, 'chaise-eilidh', '2024-04-22 13:37:59'),
(12, 4, 1, 'Chaise MacLeod', 'Chaise MacLeod. Morbi eget magna vel risus ultrices laoreet. Aliquam erat volutpat. Nulla facilisi. Phasellus.', 348.03, 9, 'chaise-macleod', '2024-01-31 08:59:03'),
(13, 4, 2, 'Chaise MacKenzie', 'Chaise MacKenzie. Cras id ipsum eget libero commodo lobortis. Donec mattis libero nec velit sagittis.', 275.35, 6, 'chaise-mackenzie', '2024-03-11 15:22:41'),
(14, 4, 3, 'Chaise Finlay', 'Chaise Finlay. Fusce vel magna vel magna finibus condimentum. Proin id magna vel magna finibus.', 149.29, 1, 'chaise-finlay', '2024-05-18 10:48:12'),
(15, 4, 1, 'Chaise Morag', 'Chaise Morag. Nullam efficitur metus ut augue bibendum, vel mattis mi interdum. Donec eget.', 208.45, 4, 'chaise-morag', '2024-02-27 23:06:34'),
(16, 4, 2, 'Chaise Cameron', 'Chaise Cameron. Aenean vel magna vel magna finibus condimentum. Pellentesque habitant morbi tristique senectus et.', 66.88, 3, 'chaise-cameron', '2024-04-13 14:39:20'),
(17, 5, 1, 'Tabouret Lachlan', 'Tabouret Lachlan. Praesent eu urna nec magna tincidunt condimentum. Etiam ac augue vel magna finibus.', 93.12, 8, 'tabouret-lachlan', '2024-03-01 11:17:28'),
(18, 5, 2, 'Tabouret MacGregor', 'Tabouret MacGregor. Sed ut risus nec neque efficitur molestie. Nulla facilisi. Vivamus euismod magna vel.', 127.96, 5, 'tabouret-macgregor', '2024-05-22 16:52:09'),
(19, 5, 3, 'Tabouret Eilidh', 'Tabouret Eilidh. Morbi eget magna vel risus ultrices laoreet. Aliquam erat volutpat. Nulla facilisi. Phasellus.', 123.2, 2, 'tabouret-eilidh', '2024-04-06 20:14:45'),
(20, 5, 1, 'Tabouret MacLeod', 'Tabouret MacLeod. Cras id ipsum eget libero commodo lobortis. Donec mattis libero nec velit sagittis.', 119.14, 9, 'tabouret-macleod', '2024-01-12 09:31:51'),
(21, 5, 2, 'Tabouret MacKenzie', 'Tabouret MacKenzie. Nullam efficitur metus ut augue bibendum, vel mattis mi interdum. Donec eget.', 84.94, 2, 'tabouret-mackenzie', '2024-02-19 18:43:27'),
(22, 5, 3, 'Tabouret Finlay', 'Tabouret Finlay. Fusce vel magna vel magna finibus condimentum. Proin id magna vel magna finibus.', 71.58, 5, 'tabouret-finlay', '2024-03-31 13:51:02'),
(23, 5, 1, 'Tabouret Morag', 'Tabouret Morag. Nulla facilisi. Vivamus euismod magna vel magna finibus condimentum. Proin id magna vel.', 125.64, 1, 'tabouret-morag', '2024-05-12 22:28:39'),
(24, 5, 2, 'Tabouret Cameron', 'Tabouret Cameron. Nulla facilisi. Vivamus euismod magna vel magna finibus condimentum. Proin id magna vel.', 72.86, 8, 'tabouret-cameron', '2024-04-26 15:17:14'),
(25, 3, 1, 'Table Lachlan', 'Table Lachlan. Nulla facilisi. Vivamus euismod magna vel magna finibus condimentum. Proin id magna vel.', 114.71, 2, 'table-lachlan', '2024-02-04 10:42:53');

INSERT INTO `arns_line` (`id`, `arns_basket_id`, `product_id`, `quantity`) VALUES
(1, 9, 2, 1),
(2, 9, 18, 2),
(3, 10, 2, 1),
(4, 10, 3, 1);

INSERT INTO `arns_slide` (`id`, `img_name`, `file`, `name`, `description`) VALUES
(1, 'slide-1', 'uploads/img/slide/751af38c04e3a4e6f44dc8862f1e7e644e59c85e.jpg', 'Slide 1', NULL),
(2, 'slide-2', 'uploads/img/slide/9b81c9a7bdeb9420b765a1169920751019a1d24a.jpg', 'Slide 2', NULL),
(3, 'slide-3', 'uploads/img/slide/6f75d40afa0660531bdcc300e8106890bb37bd9a.jpg', 'Slide 3', NULL);
