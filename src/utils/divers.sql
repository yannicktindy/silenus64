UPDATE `user` SET `roles` = '["ROLE_USER", "ROLE_ADMIN", "ROLE_SUPER_ADMIN"]' WHERE `user`.`id` = 1;

-- 1. Supprimer les adresses associées à l'utilisateur
DELETE FROM arns_adress
WHERE user_id = 13;

-- 2. Supprimer les paniers associés à l'utilisateur
DELETE FROM arns_basket
WHERE user_id = 13;

-- 3. Supprimer l'utilisateur
DELETE FROM user
WHERE id = 13;




