-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : lun. 27 mai 2024 à 12:54
-- Version du serveur : 8.0.31
-- Version de PHP : 8.1.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `silenus64`
--

-- --------------------------------------------------------

--
-- Structure de la table `arns_adress`
--

DROP TABLE IF EXISTS `arns_adress`;
CREATE TABLE IF NOT EXISTS `arns_adress` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `number` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zip` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_86AC9EB7A76ED395` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `arns_adress`
--

INSERT INTO `arns_adress` (`id`, `user_id`, `number`, `name`, `zip`, `city`, `country`) VALUES
(1, 1, '117', 'rue annatole france', '69100', 'Villeurbanne', 'France');

-- --------------------------------------------------------

--
-- Structure de la table `arns_basket`
--

DROP TABLE IF EXISTS `arns_basket`;
CREATE TABLE IF NOT EXISTS `arns_basket` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `arns_adress_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_F8060972A76ED395` (`user_id`),
  KEY `IDX_F806097296F26574` (`arns_adress_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `arns_basket`
--

INSERT INTO `arns_basket` (`id`, `user_id`, `created_at`, `status`, `arns_adress_id`) VALUES
(9, 1, '2024-05-23 19:11:21', 'Payé', NULL),
(10, 1, '2024-05-27 12:08:53', 'Payé', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `arns_category`
--

DROP TABLE IF EXISTS `arns_category`;
CREATE TABLE IF NOT EXISTS `arns_category` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img_name` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `arns_category`
--

INSERT INTO `arns_category` (`id`, `name`, `img_name`, `file`, `slug`) VALUES
(3, 'Table', 'imagename', 'uploads/img/category/16f3deb6c9f7649fa4b443f637a297348ce0311c.jpg', 'table'),
(4, 'Chaise', 'chaise', 'uploads/img/category/14cebddcb1b7c24d6c43137ed2ce345584bb4394.jpg', 'chaise'),
(5, 'Tabouret', 'tabouret', 'uploads/img/category/c910ecc9836c2ce79fd44bcdf1afc90d22b91452.jpg', 'tabouret');

-- --------------------------------------------------------

--
-- Structure de la table `arns_line`
--

DROP TABLE IF EXISTS `arns_line`;
CREATE TABLE IF NOT EXISTS `arns_line` (
  `id` int NOT NULL AUTO_INCREMENT,
  `arns_basket_id` int DEFAULT NULL,
  `product_id` int DEFAULT NULL,
  `quantity` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_5F486C83995678A` (`arns_basket_id`),
  KEY `IDX_5F486C834584665A` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `arns_line`
--

INSERT INTO `arns_line` (`id`, `arns_basket_id`, `product_id`, `quantity`) VALUES
(1, 9, 2, 1),
(2, 9, 18, 2),
(3, 10, 2, 1),
(4, 10, 3, 1);

-- --------------------------------------------------------

--
-- Structure de la table `arns_matter`
--

DROP TABLE IF EXISTS `arns_matter`;
CREATE TABLE IF NOT EXISTS `arns_matter` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img_name` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `arns_matter`
--

INSERT INTO `arns_matter` (`id`, `name`, `img_name`, `file`, `slug`) VALUES
(1, 'Metal', 'metal', 'uploads/img/category/2fd2a5ee26e3dc3ce84be33bad30d0c1d63930cc.jpg', 'metal'),
(2, 'Bois Clair', 'bois-clair', 'uploads/img/category/77c37df64481d7714abd90ffcb671958dc802f1a.jpg', 'boisclair'),
(3, 'Bois Sombre', 'bois-sombre', 'uploads/img/category/1160f15c008f64de386fd27f5293f8cd7d225edc.jpg', 'boissombre');

-- --------------------------------------------------------

--
-- Structure de la table `arns_product`
--

DROP TABLE IF EXISTS `arns_product`;
CREATE TABLE IF NOT EXISTS `arns_product` (
  `id` int NOT NULL AUTO_INCREMENT,
  `category_id` int DEFAULT NULL,
  `matter_id` int DEFAULT NULL,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `quantity` int NOT NULL,
  `slug` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`id`),
  KEY `IDX_AA4CFC5012469DE2` (`category_id`),
  KEY `IDX_AA4CFC50D614E59F` (`matter_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `arns_product`
--

INSERT INTO `arns_product` (`id`, `category_id`, `matter_id`, `name`, `description`, `price`, `quantity`, `slug`, `created_at`) VALUES
(2, 3, 2, 'Table MacGregor', 'Table MacGregor', 588.91, 3, 'table-macgregor', NULL),
(3, 3, 3, 'Table Eilidh', 'Table Eilidh', 256.87, 10, 'table-eilidh', NULL),
(4, 3, 1, 'Table MacLeod', 'Table MacLeod', 180.45, 4, 'table-macleod', NULL),
(5, 3, 2, 'Table MacKenzie', 'Table MacKenzie', 216.68, 1, 'table-mackenzie', NULL),
(6, 3, 3, 'Table Finlay', 'Table Finlay', 592.79, 6, 'table-finlay', NULL),
(7, 3, 1, 'Table Morag', 'Table Morag', 412.89, 7, 'table-morag', NULL),
(8, 3, 2, 'Table Cameron', 'Table Cameron', 109.88, 4, 'table-cameron', NULL),
(9, 4, 1, 'Chaise Lachlan', 'Chaise Lachlan', 272.97, 8, 'chaise-lachlan', NULL),
(10, 4, 2, 'Chaise MacGregor', 'Chaise MacGregor', 333.03, 10, 'chaise-macgregor', NULL),
(11, 4, 3, 'Chaise Eilidh', 'Chaise Eilidh', 120.8, 9, 'chaise-eilidh', NULL),
(12, 4, 1, 'Chaise MacLeod', 'Chaise MacLeod', 348.03, 9, 'chaise-macleod', NULL),
(13, 4, 2, 'Chaise MacKenzie', 'Chaise MacKenzie', 275.35, 6, 'chaise-mackenzie', NULL),
(14, 4, 3, 'Chaise Finlay', 'Chaise Finlay', 149.29, 1, 'chaise-finlay', NULL),
(15, 4, 1, 'Chaise Morag', 'Chaise Morag', 208.45, 4, 'chaise-morag', NULL),
(16, 4, 2, 'Chaise Cameron', 'Chaise Cameron', 66.88, 3, 'chaise-cameron', NULL),
(17, 5, 1, 'Tabouret Lachlan', 'Tabouret Lachlan', 93.12, 8, 'tabouret-lachlan', NULL),
(18, 5, 2, 'Tabouret MacGregor', 'Tabouret MacGregor', 127.96, 5, 'tabouret-macgregor', NULL),
(19, 5, 3, 'Tabouret Eilidh', 'Tabouret Eilidh', 123.2, 2, 'tabouret-eilidh', NULL),
(20, 5, 1, 'Tabouret MacLeod', 'Tabouret MacLeod', 119.14, 9, 'tabouret-macleod', NULL),
(21, 5, 2, 'Tabouret MacKenzie', 'Tabouret MacKenzie', 84.94, 2, 'tabouret-mackenzie', NULL),
(22, 5, 3, 'Tabouret Finlay', 'Tabouret Finlay', 71.58, 5, 'tabouret-finlay', NULL),
(23, 5, 1, 'Tabouret Morag', 'Tabouret Morag', 125.64, 1, 'tabouret-morag', NULL),
(24, 5, 2, 'Tabouret Cameron', 'Tabouret Cameron', 72.86, 8, 'tabouret-cameron', NULL),
(25, 3, 1, 'Table Lachlan', 'Table Lachlan', 114.71, 2, 'table-lachlan', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `arns_product_image`
--

DROP TABLE IF EXISTS `arns_product_image`;
CREATE TABLE IF NOT EXISTS `arns_product_image` (
  `id` int NOT NULL AUTO_INCREMENT,
  `product_id` int DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_CB62625B4584665A` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `arns_slide`
--

DROP TABLE IF EXISTS `arns_slide`;
CREATE TABLE IF NOT EXISTS `arns_slide` (
  `id` int NOT NULL AUTO_INCREMENT,
  `img_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `arns_slide`
--

INSERT INTO `arns_slide` (`id`, `img_name`, `file`, `name`, `description`) VALUES
(1, 'slide-1', 'uploads/img/slide/751af38c04e3a4e6f44dc8862f1e7e644e59c85e.jpg', 'Slide 1', NULL),
(2, 'slide-2', 'uploads/img/slide/9b81c9a7bdeb9420b765a1169920751019a1d24a.jpg', 'Slide 2', NULL),
(3, 'slide-3', 'uploads/img/slide/6f75d40afa0660531bdcc300e8106890bb37bd9a.jpg', 'Slide 3', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `doctrine_migration_versions`
--

DROP TABLE IF EXISTS `doctrine_migration_versions`;
CREATE TABLE IF NOT EXISTS `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8mb3_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

--
-- Déchargement des données de la table `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20240508110756', '2024-05-08 11:08:09', 626),
('DoctrineMigrations\\Version20240508145342', '2024-05-08 14:53:51', 134),
('DoctrineMigrations\\Version20240514060746', '2024-05-14 06:08:05', 356),
('DoctrineMigrations\\Version20240522181733', '2024-05-22 18:17:46', 119),
('DoctrineMigrations\\Version20240527122230', '2024-05-27 12:22:41', 157);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` json NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_IDENTIFIER_EMAIL` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `email`, `roles`, `password`, `firstname`, `lastname`, `mobile`) VALUES
(1, 'yannicktindy@gmail.com', '[\"ROLE_USER\", \"ROLE_ADMIN\", \"ROLE_SUPER_ADMIN\"]', '$2y$13$Ln8vkxXIUYj2RCfbuXGnDuGyNXzv29M/1d7syqwFcuG3H6QarwCxC', 'sam', 'raven', NULL);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `arns_adress`
--
ALTER TABLE `arns_adress`
  ADD CONSTRAINT `FK_86AC9EB7A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `arns_basket`
--
ALTER TABLE `arns_basket`
  ADD CONSTRAINT `FK_F806097296F26574` FOREIGN KEY (`arns_adress_id`) REFERENCES `arns_adress` (`id`),
  ADD CONSTRAINT `FK_F8060972A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `arns_line`
--
ALTER TABLE `arns_line`
  ADD CONSTRAINT `FK_5F486C834584665A` FOREIGN KEY (`product_id`) REFERENCES `arns_product` (`id`),
  ADD CONSTRAINT `FK_5F486C83995678A` FOREIGN KEY (`arns_basket_id`) REFERENCES `arns_basket` (`id`);

--
-- Contraintes pour la table `arns_product`
--
ALTER TABLE `arns_product`
  ADD CONSTRAINT `FK_AA4CFC5012469DE2` FOREIGN KEY (`category_id`) REFERENCES `arns_category` (`id`),
  ADD CONSTRAINT `FK_AA4CFC50D614E59F` FOREIGN KEY (`matter_id`) REFERENCES `arns_matter` (`id`);

--
-- Contraintes pour la table `arns_product_image`
--
ALTER TABLE `arns_product_image`
  ADD CONSTRAINT `FK_CB62625B4584665A` FOREIGN KEY (`product_id`) REFERENCES `arns_product` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
