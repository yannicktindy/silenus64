-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mar. 14 mai 2024 à 06:59
-- Version du serveur : 8.0.31
-- Version de PHP : 8.1.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `silenus64`
--

-- --------------------------------------------------------

--
-- Structure de la table `arns_adress`
--

DROP TABLE IF EXISTS `arns_adress`;
CREATE TABLE IF NOT EXISTS `arns_adress` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `number` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zip` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_86AC9EB7A76ED395` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `arns_basket`
--

DROP TABLE IF EXISTS `arns_basket`;
CREATE TABLE IF NOT EXISTS `arns_basket` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_F8060972A76ED395` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `arns_category`
--

DROP TABLE IF EXISTS `arns_category`;
CREATE TABLE IF NOT EXISTS `arns_category` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img_name` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `arns_category`
--

INSERT INTO `arns_category` (`id`, `name`, `img_name`, `file`, `slug`) VALUES
(3, 'Table', 'imagename', 'uploads/img/category/16f3deb6c9f7649fa4b443f637a297348ce0311c.jpg', 'table'),
(4, 'Chaise', 'chaise', 'uploads/img/category/14cebddcb1b7c24d6c43137ed2ce345584bb4394.jpg', 'chaise'),
(5, 'Tabouret', 'tabouret', 'uploads/img/category/c910ecc9836c2ce79fd44bcdf1afc90d22b91452.jpg', 'tabouret');

-- --------------------------------------------------------

--
-- Structure de la table `arns_line`
--

DROP TABLE IF EXISTS `arns_line`;
CREATE TABLE IF NOT EXISTS `arns_line` (
  `id` int NOT NULL AUTO_INCREMENT,
  `arns_basket_id` int DEFAULT NULL,
  `product_id` int DEFAULT NULL,
  `quantity` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_5F486C83995678A` (`arns_basket_id`),
  KEY `IDX_5F486C834584665A` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `arns_matter`
--

DROP TABLE IF EXISTS `arns_matter`;
CREATE TABLE IF NOT EXISTS `arns_matter` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img_name` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `arns_matter`
--

INSERT INTO `arns_matter` (`id`, `name`, `img_name`, `file`, `slug`) VALUES
(1, 'Metal', 'metal', 'uploads/img/category/2fd2a5ee26e3dc3ce84be33bad30d0c1d63930cc.jpg', 'metal'),
(2, 'Bois Clair', 'bois-clair', 'uploads/img/category/c600abec8a0be78f67f7304adc281d6027c5588f.jpg', 'bois-clair'),
(3, 'Bois Sombre', 'bois-sombre', 'uploads/img/category/da60c079cc980a6b681bdafd0db10458157777e4.jpg', 'bois-sombre');

-- --------------------------------------------------------

--
-- Structure de la table `arns_product`
--

DROP TABLE IF EXISTS `arns_product`;
CREATE TABLE IF NOT EXISTS `arns_product` (
  `id` int NOT NULL AUTO_INCREMENT,
  `category_id` int DEFAULT NULL,
  `matter_id` int DEFAULT NULL,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `quantity` int NOT NULL,
  `slug` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_AA4CFC5012469DE2` (`category_id`),
  KEY `IDX_AA4CFC50D614E59F` (`matter_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `arns_product`
--

INSERT INTO `arns_product` (`id`, `category_id`, `matter_id`, `name`, `description`, `price`, `quantity`, `slug`) VALUES
(1, NULL, NULL, 'Chaise test', 'dfgfgdfgdfgggdfg', 50.23, 12, 'chaise-test');

-- --------------------------------------------------------

--
-- Structure de la table `arns_product_image`
--

DROP TABLE IF EXISTS `arns_product_image`;
CREATE TABLE IF NOT EXISTS `arns_product_image` (
  `id` int NOT NULL AUTO_INCREMENT,
  `product_id` int DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_CB62625B4584665A` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `arns_product_image`
--

INSERT INTO `arns_product_image` (`id`, `product_id`, `name`, `file`) VALUES
(1, 1, 'chaise', 'uploads/img/product/a9c60e8c472b1d7a15bebc96379301d88524151e.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `arns_slide`
--

DROP TABLE IF EXISTS `arns_slide`;
CREATE TABLE IF NOT EXISTS `arns_slide` (
  `id` int NOT NULL AUTO_INCREMENT,
  `img_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `arns_slide`
--

INSERT INTO `arns_slide` (`id`, `img_name`, `file`, `name`, `description`) VALUES
(1, 'slide-1', 'uploads/img/slide/751af38c04e3a4e6f44dc8862f1e7e644e59c85e.jpg', 'Slide 1', NULL),
(2, 'slide-2', 'uploads/img/slide/9b81c9a7bdeb9420b765a1169920751019a1d24a.jpg', 'Slide 2', NULL),
(3, 'slide-3', 'uploads/img/slide/6f75d40afa0660531bdcc300e8106890bb37bd9a.jpg', 'Slide 3', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `doctrine_migration_versions`
--

DROP TABLE IF EXISTS `doctrine_migration_versions`;
CREATE TABLE IF NOT EXISTS `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8mb3_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

--
-- Déchargement des données de la table `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20240508110756', '2024-05-08 11:08:09', 626),
('DoctrineMigrations\\Version20240508145342', '2024-05-08 14:53:51', 134),
('DoctrineMigrations\\Version20240514060746', '2024-05-14 06:08:05', 356);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` json NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_IDENTIFIER_EMAIL` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `arns_adress`
--
ALTER TABLE `arns_adress`
  ADD CONSTRAINT `FK_86AC9EB7A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `arns_basket`
--
ALTER TABLE `arns_basket`
  ADD CONSTRAINT `FK_F8060972A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `arns_line`
--
ALTER TABLE `arns_line`
  ADD CONSTRAINT `FK_5F486C834584665A` FOREIGN KEY (`product_id`) REFERENCES `arns_product` (`id`),
  ADD CONSTRAINT `FK_5F486C83995678A` FOREIGN KEY (`arns_basket_id`) REFERENCES `arns_basket` (`id`);

--
-- Contraintes pour la table `arns_product`
--
ALTER TABLE `arns_product`
  ADD CONSTRAINT `FK_AA4CFC5012469DE2` FOREIGN KEY (`category_id`) REFERENCES `arns_category` (`id`),
  ADD CONSTRAINT `FK_AA4CFC50D614E59F` FOREIGN KEY (`matter_id`) REFERENCES `arns_matter` (`id`);

--
-- Contraintes pour la table `arns_product_image`
--
ALTER TABLE `arns_product_image`
  ADD CONSTRAINT `FK_CB62625B4584665A` FOREIGN KEY (`product_id`) REFERENCES `arns_product` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
