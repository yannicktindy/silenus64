INSERT INTO `arns_category` (`id`, `name`, `img_name`, `file`, `slug`) VALUES
(3, 'Table', 'imagename', 'uploads/img/category/16f3deb6c9f7649fa4b443f637a297348ce0311c.jpg', 'table'),
(4, 'Chaise', 'chaise', 'uploads/img/category/14cebddcb1b7c24d6c43137ed2ce345584bb4394.jpg', 'chaise'),
(5, 'Tabouret', 'tabouret', 'uploads/img/category/c910ecc9836c2ce79fd44bcdf1afc90d22b91452.jpg', 'tabouret');


INSERT INTO `arns_matter` (`id`, `name`, `img_name`, `file`, `slug`) VALUES
(1, 'Metal', 'metal', 'uploads/img/category/2fd2a5ee26e3dc3ce84be33bad30d0c1d63930cc.jpg', 'metal'),
(2, 'Bois Clair', 'bois-clair', 'uploads/img/category/c600abec8a0be78f67f7304adc281d6027c5588f.jpg', 'bois-clair'),
(3, 'Bois Sombre', 'bois-sombre', 'uploads/img/category/da60c079cc980a6b681bdafd0db10458157777e4.jpg', 'bois-sombre');


INSERT INTO `arns_product` (`id`, `category_id`, `matter_id`, `name`, `description`, `price`, `quantity`, `slug`) VALUES
(1, 3, 1, 'Table Lachlan', 'Table Lachlan', ROUND(RAND() * 500 + 100, 2), FLOOR(RAND() * 13), 'table-lachlan'),
(2, 3, 2, 'Table MacGregor', 'Table MacGregor', ROUND(RAND() * 500 + 100, 2), FLOOR(RAND() * 13), 'table-macgregor'),
(3, 3, 3, 'Table Eilidh', 'Table Eilidh', ROUND(RAND() * 500 + 100, 2), FLOOR(RAND() * 13), 'table-eilidh'),
(4, 3, 1, 'Table MacLeod', 'Table MacLeod', ROUND(RAND() * 500 + 100, 2), FLOOR(RAND() * 13), 'table-macleod'),
(5, 3, 2, 'Table MacKenzie', 'Table MacKenzie', ROUND(RAND() * 500 + 100, 2), FLOOR(RAND() * 13), 'table-mackenzie'),
(6, 3, 3, 'Table Finlay', 'Table Finlay', ROUND(RAND() * 500 + 100, 2), FLOOR(RAND() * 13), 'table-finlay'),
(7, 3, 1, 'Table Morag', 'Table Morag', ROUND(RAND() * 500 + 100, 2), FLOOR(RAND() * 13), 'table-morag'),
(8, 3, 2, 'Table Cameron', 'Table Cameron', ROUND(RAND() * 500 + 100, 2), FLOOR(RAND() * 13), 'table-cameron'),
(9, 4, 1, 'Chaise Lachlan', 'Chaise Lachlan', ROUND(RAND() * 300 + 50, 2), FLOOR(RAND() * 13), 'chaise-lachlan'),
(10, 4, 2, 'Chaise MacGregor', 'Chaise MacGregor', ROUND(RAND() * 300 + 50, 2), FLOOR(RAND() * 13), 'chaise-macgregor'),
(11, 4, 3, 'Chaise Eilidh', 'Chaise Eilidh', ROUND(RAND() * 300 + 50, 2), FLOOR(RAND() * 13), 'chaise-eilidh'),
(12, 4, 1, 'Chaise MacLeod', 'Chaise MacLeod', ROUND(RAND() * 300 + 50, 2), FLOOR(RAND() * 13), 'chaise-macleod'),
(13, 4, 2, 'Chaise MacKenzie', 'Chaise MacKenzie', ROUND(RAND() * 300 + 50, 2), FLOOR(RAND() * 13), 'chaise-mackenzie'),
(14, 4, 3, 'Chaise Finlay', 'Chaise Finlay', ROUND(RAND() * 300 + 50, 2), FLOOR(RAND() * 13), 'chaise-finlay'),
(15, 4, 1, 'Chaise Morag', 'Chaise Morag', ROUND(RAND() * 300 + 50, 2), FLOOR(RAND() * 13), 'chaise-morag'),
(16, 4, 2, 'Chaise Cameron', 'Chaise Cameron', ROUND(RAND() * 300 + 50, 2), FLOOR(RAND() * 13), 'chaise-cameron'),
(17, 5, 1, 'Tabouret Lachlan', 'Tabouret Lachlan', ROUND(RAND() * 200 + 30, 2), FLOOR(RAND() * 13), 'tabouret-lachlan'),
(18, 5, 2, 'Tabouret MacGregor', 'Tabouret MacGregor', ROUND(RAND() * 200 + 30, 2), FLOOR(RAND() * 13), 'tabouret-macgregor'),
(19, 5, 3, 'Tabouret Eilidh', 'Tabouret Eilidh', ROUND(RAND() * 200 + 30, 2), FLOOR(RAND() * 13), 'tabouret-eilidh'),
(20, 5, 1, 'Tabouret MacLeod', 'Tabouret MacLeod', ROUND(RAND() * 200 + 30, 2), FLOOR(RAND() * 13), 'tabouret-macleod'),
(21, 5, 2, 'Tabouret MacKenzie', 'Tabouret MacKenzie', ROUND(RAND() * 200 + 30, 2), FLOOR(RAND() * 13), 'tabouret-mackenzie'),
(22, 5, 3, 'Tabouret Finlay', 'Tabouret Finlay', ROUND(RAND() * 200 + 30, 2), FLOOR(RAND() * 13), 'tabouret-finlay'),
(23, 5, 1, 'Tabouret Morag', 'Tabouret Morag', ROUND(RAND() * 200 + 30, 2), FLOOR(RAND() * 13), 'tabouret-morag'),
(24, 5, 2, 'Tabouret Cameron', 'Tabouret Cameron', ROUND(RAND() * 200 + 30, 2), FLOOR(RAND() * 13), 'tabouret-cameron');




-
INSERT INTO `arns_product_image` (`id`, `product_id`, `name`, `file`) VALUES
(1, 1, 'chaise', 'uploads/img/product/a9c60e8c472b1d7a15bebc96379301d88524151e.jpg');



INSERT INTO `arns_slide` (`id`, `img_name`, `file`, `name`, `description`) VALUES
(1, 'slide-1', 'uploads/img/slide/751af38c04e3a4e6f44dc8862f1e7e644e59c85e.jpg', 'Slide 1', NULL),
(2, 'slide-2', 'uploads/img/slide/9b81c9a7bdeb9420b765a1169920751019a1d24a.jpg', 'Slide 2', NULL),
(3, 'slide-3', 'uploads/img/slide/6f75d40afa0660531bdcc300e8106890bb37bd9a.jpg', 'Slide 3', NULL);

